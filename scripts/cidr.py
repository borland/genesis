import subprocess
from json import loads
from operator import itemgetter

bashCommand = "aws ec2 describe-subnets"
process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
subnets, error = process.communicate()

subnets = loads(subnets.decode())

avail_blocks = list(range(16))

for subnet in subnets['Subnets']:
    block = int(subnet["CidrBlock"].split('.')[2]) / 16
    avail_blocks.remove(block)

if len(avail_blocks) < 3:
    raise ValueError('No available CIDR blocks.')

subnet_blocks = avail_blocks[:3]
cidr_prefix = '.'.join(subnets['Subnets'][0]['CidrBlock'].split('.')[:2])

picked_subnets = ['{}.{}.0/20'.format(cidr_prefix, block * 16) for block in subnet_blocks]

for idx, subnet in enumerate(picked_subnets):
    print("export TF_VAR_subnet_{}='{}'".format(chr(97 + idx), subnet))

default_subnets = sorted([subnet for subnet in subnets['Subnets'] if subnet['DefaultForAz']],
                         key=itemgetter('AvailabilityZone'))

print("export GNAR_KOPS_SUBNET_ZONES='{}'".format(','.join([subnet['AvailabilityZone'] for subnet in default_subnets])))
print("export GNAR_KOPS_SUBNET_IDS='{}'".format(','.join([subnet['SubnetId'] for subnet in default_subnets])))
